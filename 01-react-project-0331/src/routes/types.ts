import type { ReactElement } from "react";
import type { RouteObject } from "react-router-dom";

export interface XMeta {
  icon?: React.ReactNode;
  title?: string | ReactElement;
}


/**
 * meta属性和hidden属性都是我们自定义的属性,用来保存当前路由的固定信息(注意和路由传参的区别)
 * meta主要保存当前路由的标题1和对应的图标,用来在侧边栏展示所有的路由列表
 * hidden:主要控制的是 当生成面包屑导航的时候,当前路由是否添加在面包屑导航上
 */
export interface XRoute extends RouteObject {
  meta?: XMeta;
  children?: XRoutes;
  hidden?: boolean;
}

export type XRoutes = XRoute[];
