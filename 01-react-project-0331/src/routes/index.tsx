// src/routes/index.tsx
import React, { lazy, Suspense, FC } from "react";
import { useRoutes, Navigate } from "react-router-dom";
import { HomeOutlined,PlusOutlined } from "@ant-design/icons";
import type { XRoutes } from "./types";

import {
  Layout,
  EmptyLayout,
  // CompLayout
} from "../layouts";
import Loading from "@comps/Loading";

import Translation from "@comps/Translation";

const Login = lazy(() => import("@pages/login"));
const Dashboard = lazy(() => import("@pages/dashboard"));
const NotFound = lazy(() => import("@pages/404"));
const HospitalList = lazy(()=>import("@pages/hospital/hospitalList"));
const HospitalSet = lazy(()=>import("@pages/hospital/hospitalSet"));
const AddOrUpdateHosSet = lazy(() => import("@pages/hospital/hospitalSet/addOrUpdateHosSet"))
const HospitalShow = lazy(()=>import("@pages/hospital/hospitalList/hospitalShow"))

const load = (Comp: FC) => {
  return (
    // 因为路由懒加载，组件需要一段网络请求时间才能加载并渲染
    // 在组件还未渲染时，fallback就生效，来渲染一个加载进度条效果
    // 当组件渲染完成时，fallback就失效了
    <Suspense fallback={<Loading />}>
      {/* 所有lazy的组件必须包裹Suspense组件，才能实现功能 */}
      <Comp />
    </Suspense>
  );
};

const routes: XRoutes = [
  {
    path: "/",
    element: <EmptyLayout />,
    children: [
      {
        path: "login",
        element: load(Login),
      },
    ],
  },
  {
    /**
 * meta属性和hidden属性都是我们自定义的属性,用来保存当前路由的固定信息(注意和路由传参的区别)
 * meta主要保存当前路由的标题1和对应的图标,用来在侧边栏展示所有的路由列表
 * hidden:主要控制的是 当生成面包屑导航的时候,当前路由是否添加在面包屑导航上
 */
    path: "/syt",
    element: <Layout />,  //layout组件
    children: [
      {
        path: "/syt/dashboard",
        meta: { icon: <HomeOutlined />, title: <Translation>route:dashboard</Translation> },
        element: load(Dashboard),
      },

      {
        path: "/syt/hospital",
        meta: { icon: <PlusOutlined />, title: "医院管理" },
        element:  < EmptyLayout/>,
        children:[
          {
            path: "/syt/hospital/hospitalList",
            meta: { title: "医院列表"},
            element:load(HospitalList),
          },
          {
            path: "/syt/hospital/hospitalShow/:id",
            meta: { title: "医院详情"},
            hidden:true,
            element:load(HospitalShow),
          },

        {  
          path: "/syt/hospital/hospitalSet",
          meta: { title: "医院设置"},
          element: load(HospitalSet),
        },
        {
          path: "/syt/hospital/addhosset",
          meta: { title: "添加医院" },
          hidden: true,
          element: load(AddOrUpdateHosSet),
        },
        {
          path: "/syt/hospital/updatehosset/:id",
          meta: { title: "更新医院" },
          hidden: true,
          element: load(AddOrUpdateHosSet),
        }
        ]
      },


    ],
  },

  {
    path: "/404",
    element: load(NotFound),
  },
  {
    path: "*",
    element: <Navigate to="/404" />,
  },
];

// 渲染路由
// 注意：首字母必须大写
export const RenderRoutes = () => {
  // react-router-dom的新增语法。不用自己遍历了，它帮我们遍历生成
  return useRoutes(routes);
};

// 找到要渲染成左侧菜单的路由
export const findSideBarRoutes = () => {
  const currentIndex = routes.findIndex((route) => route.path === "/syt");
  return routes[currentIndex].children as XRoutes;
};

export default routes;
