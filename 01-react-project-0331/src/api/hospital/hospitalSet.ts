import {request} from '@/utils/http';
import {Key  } from "react";

import {
    addHospitalParamType,
    getHospitalSetListItemsType,
    getHospitalSetListParamsType,
    getHospitalSetListReturnType,
    updateHospitalParamType
} from './model/hospitalSetTypes';

//1.医院设置中,获取分页列表
export const getHospitalSetListRequest=({page,limit,hosname,hoscode}:getHospitalSetListParamsType)=>{
    return request.get<any,getHospitalSetListReturnType>(`/admin/hosp/hospitalSet/${page}/${limit}`,{
    params:{
        hosname,
        hoscode
    }
    })
}

//2.医院设置中,新增医院请求
export const addHospitalRequest = (data:addHospitalParamType)=>{
    return request.post<any,null>("/admin/hosp/hospitalSet/save",data)
}

//3.在医院设置中,根据id修改数据后,再次提交修改
export const getHosSetFormIdRequest=(id:string)=>{
    return request.get<any,getHospitalSetListItemsType>(`/admin/hosp/hospitalSet/get/${id}`)
}

//4.医院设置中,根据id修改数据后,再次提交修改
export const updateHosSetFormRequest=(data:updateHospitalParamType)=>{
    return request.put<any,null>("/admin/hosp/hospitalSet/update", data)
}

//5.医院设置中,根据id删除数据
export const deleteHosSetFormIdRequest = (id:number)=>{
    return request.delete<any,null>(`/admin/hosp/hospitalSet/remove/${id}`)
}

//6.医院设置中,批量删除数据
export const batchDeleteHosRequest = (ids:Key[])=>{
    return request.delete<any,null>(`/admin/hosp/hospitalSet/batchRemove`,{
        data : ids
    })

}