import {request} from "@/utils/http"
import type  { getHosListSearchParamsType ,hospitalListReqReturnType,getProvinceReturnType, hospitalDetailReturnType} from "./model/hospitalListTypes"

//1.获取医院列表数据的请求
export const getHospitalListRequest = (page:number,limit:number,searchParams:getHosListSearchParamsType)=>{
    return request.get<any,hospitalListReqReturnType>(`/admin/hosp/hospital/${page}/${limit}`,{
                                                                                //$path传参                                                                                                                                                       
        params:searchParams   //路由传参
    })
}

//2.请求所有的省份
export const getProvinceRequest=()=>{
    return request.get<any,getProvinceReturnType>("/admin/cmn/dict/findByDictCode/province")
}

//3.请求城市
export const  getCityOrDistrictRequest  = (value:string)=>{
    return  request.get<any,getProvinceReturnType>(`/admin/cmn/dict/findByParentId/${value}`)
}

//4.请求医院详情信息
export const getHosListsDetailRequest=(id:string)=>{
    return request.get<any,hospitalDetailReturnType>(`/admin/hosp/hospital/show/${id}`)
}

//5.医院上下线
export const changeStatusRequest = (id:string,status:0|1)=>{
    return request.get<any,null>(`/admin/hosp/hospital/updateStatus/${id}/${status}`)

}