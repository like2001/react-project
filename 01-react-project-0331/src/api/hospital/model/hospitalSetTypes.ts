import { type } from "os";

//1.医院设置中,获取分页列表请求的参数类型
export interface getHospitalSetListParamsType {
  page: number;
  limit: number;
  hosname?: string;
  hoscode?: string;
}

//2.医院设置中,获取分页列表请求的返回值类型

//2.1返回值的records中的每一项的类型
export interface getHospitalSetListItemsType {
  id: number;
  createTime: string;
  updateTime: string;
  isDeleted: number;
  param: object;
  hosname: string;
  hoscode: string;
  apiUrl: string;
  signKey: string;
  contactsName: string;
  contactsPhone: string;
  status: 0 | 1;
}

//2.2 返回值的Records这个数组的类型
export type getHospitalSetListRecordsType = getHospitalSetListItemsType[];

//2.3返回的完整类型
export interface getHospitalSetListReturnType {
  records: getHospitalSetListRecordsType;
  total: number;
  size: number;
  current: number;
  orders: any[];
  hitCount: boolean;
  searchCount: boolean;
  pages: number;
}

//3.新增医院api请求的参数类型,返回值类型是null,不需要定义
export interface addHospitalParamType{
  apiUrl:string,
  contacTSName:string,
  contactsPhone:string,
  hosname:string,
  hoscode:string
}

//4修改医院api请求的参数类型,比新增的时候多一个id
export interface updateHospitalParamType extends addHospitalParamType{
  id:string
}