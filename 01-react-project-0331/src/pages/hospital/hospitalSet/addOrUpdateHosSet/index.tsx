import React, { useEffect } from 'react'
import { Button, Form, Input, Card ,message} from 'antd';
import style from '../index.module.css'
import { useNavigate, useParams } from 'react-router-dom';
import { addHospitalRequest, getHosSetFormIdRequest ,updateHosSetFormRequest} from '@/api/hospital/hospitalSet';


import type { addHospitalParamType } from '@/api/hospital/model/hospitalSetTypes';

export default function AddOrUpdateHosSet() {

  const navigate = useNavigate()
  //通过-Form组件上的一个useForm方法,可以创建实例,然后在某一个Form组件上添加一个form属性,值为当前实例,那么Form表单和当前的form实例就关联起来了
  const [form] = Form.useForm()

  const {id} = useParams()
  console.log(id,"id")

  useEffect(()=>{
    //因为组件复用,在新增医院的时候不需要刚进入就发送请求,所以我们要判断当前是否有id
    if(id){
      const getHosDetail = async()=>{
        //发送请求
        const result = await getHosSetFormIdRequest(id);
        console.log(result,"result");

        //Form实例上有一个setFieldValue方法,可以把一个对象的值通过key和表单的name一一对应的形式添加到表单里
        form.setFieldsValue(result)
      }
      getHosDetail();
    }
  })





  //1.新增医院的回调函数
const addHosFinishHandle = async(filedValue:addHospitalParamType)=>{

  if(id){
    //更新请求
    await  updateHosSetFormRequest({
      ...filedValue,
      id
    })
      //修改成功以后返回医院设置页面
      navigate("/syt/hospital/hospitalSet")
    message.success('修改数据成功');

  }else{
      //发送新增请求
  await addHospitalRequest(filedValue)

  //新增成功后返回医院设置页面
  navigate("/syt/hospital/hospitalSet")
  message.success('新增数据成功');
}
}

//2.设置点击返回事件回调函数
const goBackHandle = () =>{
  navigate(-1)
}

  return (
    <Card>
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 3 }}
        wrapperCol={{ span: 18 }}
        onFinish={addHosFinishHandle}
      >
        <Form.Item
          label="医院名称"
          name="hosname"
          rules={[{
            required:true,
            message:'请输入医院名称',
            transform(value){
              return value.trim()
            }
          }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="医院编号"
          name="hoscode"
          rules={[{ required: true, message: '请输入医院编号' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="api基础路径"
          name="apiUrl"
          rules={[{ required: true, message: '请输入api基础路径' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="联系人姓名"
          name="contactsName"
          rules={[{ required: true, message: '请输入联系人姓名' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="联系人手机"
          name="contactsPhone"
          rules={[{ required: true, message: '请输入联系人手机' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          wrapperCol={{ offset: 3 }}
        >
          <Button type="primary" htmlType="submit" className={style.mr}>
            提交
          </Button>
          <Button type="default" htmlType="button"  onClick={goBackHandle}>
            返回
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
}

