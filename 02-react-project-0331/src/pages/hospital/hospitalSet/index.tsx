import React, { useEffect, useState,Key } from 'react'
import {Button,Form,Input,Card,message,Tooltip} from "antd"
import {Table} from "antd";
import style from "./index.module.css"


import {
    SearchOutlined,
    EditOutlined,
    DeleteOutlined
} from '@ant-design/icons'

import { useNavigate } from 'react-router-dom';
//引入类型
import type {getHospitalSetListRecordsType,getHospitalSetListItemsType} from '@/api/hospital/model/hospitalSetTypes'
import type {searchHosFinishedPraType} from './types'

//引入api中请求的方法
import {getHospitalSetListRequest,deleteHosSetFormIdRequest,batchDeleteHosRequest} from "@/api/hospital/hospitalSet"

export default function HospitalSet() {
  //使用编程式路由导航hook
  const navigate = useNavigate()

  //1.data是表格的数据,每一个对象中的key和colums中的dataIndex一一对象展示数据
  const [tableData,setTableData] = useState<getHospitalSetListRecordsType>([])

  //2.定义current(当前页码)的state[useState的泛型根据我们传入的参数推论出来了]
  const [current,setCurrent] = useState(1)

  //3.定义total(总个数的state)
  const [total,setTotal] = useState(0)

  //4.定义pageSize(每页的数量)
  const [pageSize,setPageSize] = useState(15)

  //5.加载视图设置,定义isLoading
  const [isLoading,setIsLoading] = useState(false)

  //6.查询的每一条数据,一开始为空,
  const [fieldValue,setFieldValue] = useState<searchHosFinishedPraType>({})

  //7.定义一个state,专门用来触发useEffect的执行,重新加载
  const [reRender,setReRender] = useState(true)

  //8.定义一个控制批量删除按钮的一个状态
  const [isSelect,setIsSelect] = useState(true)

  //9.定义一个 保存批量选中的ids的状态
  const [batchSelectIds,setBatchSelectIds] = useState<Key[]>([])

    //当组件开始加载的时候,开始请求分页列表的数据
    //注意:异步函数不能作为useEffect的参数,因为书写因为书写useEffect可能会书写return一个函数起到清理作用,如果是异步函数,则return就是异步的了

    useEffect(()=>{
      //3.请求开始之前,进入页面之前
      setIsLoading(true)

        //封装一个请求,后面调用
        const getHospitalSetList=async()=>{
            const result = await getHospitalSetListRequest({
            page:current,
            limit:pageSize,
            ...fieldValue
            })
            // console.log(result,"result");

            //1.把得到的结果中records数据(医院列表的数组)设置给state
            setTableData(result.records)

            //2.把得到的结果中,数据的总长度设置给state
            setTotal(result.total)

            //4.请求完成后,把loading改为false
            setIsLoading(false)
        }
        getHospitalSetList()
    },[current,pageSize,fieldValue,reRender])
  //colums是控制表格的字段有哪些,title属性就是当前的字段名称,dataIndex就是用来和将来的数据进行一一对应的
  //render函数的第三个参数是当前的数据的下标
  //正常要求columns中要有key 但是如果书写了dataIndex 则可以省略key
  const columns = [
    {
      title: "序号",
      width: 60,
      dataIndex: 'index',
      render(_: any, __: any, index: number) {
        return index + 1;
      },
    },
    {
      title: "医院名称",
      dataIndex: "hosname",
    },
    {
      title: "医院编号",
      dataIndex: "hoscode",
    },
    {
      title: "api基础路径",
      dataIndex: "apiUrl",
    },
    {
      title: "签名",
      dataIndex: "signKey",
    },
    {
      title: "联系人姓名",
      dataIndex: "contactsName",
    },
    {
      title: "联系人手机",
      dataIndex: "contactsPhone",
    },
    {
      title: "操作",
      width:120,//值是number
      fixed: "right" as "right",
      dataIndex:"do",
      render(_:any,data:getHospitalSetListItemsType) {
        const {id} = data
        return (
          <div>
            <Tooltip title="修改医院数据">
            <Button
              type="primary"
              icon={<EditOutlined />}
              className={style.mr}
              onClick={()=>{navigate(`/syt/hospital/updatehosset/${id}`)}}
            ></Button></Tooltip>

            <Tooltip title="删除医院数据">
            <Button 
            type="primary" 
            danger icon={<DeleteOutlined />}
            onClick={deleteHosHandle(id)}
            ></Button></Tooltip>
          </div>
        );
      },
    },
  ];

    //事件回调函数
    //1.点击页码后以后的事件回调函数
    const pageChangeHandle = (current:number,pageSize:number)=>{
      //参数1current:当前点击的页码,参数2 pagesize:每页条数
      //点击以后给state设置page和pagesize的值
      setCurrent(current)
      setPageSize(pageSize)
    }

    //2.点击查询提交表单的事件回调函数
    const searchHosFinishHandle = (FieldValue:searchHosFinishedPraType)=>{
      //参数是收集当前表单的值 组成的对象
      setFieldValue(FieldValue)
    }

    //3.添加医院事件回调函数
    const addHosHandle=()=>{
      //编程式路由导航跳转
      navigate("/syt/hospital/addhosset")
    }

    //4.删除单个医院的事件回调函数
    const deleteHosHandle = (id:number)=>{
      return async()=>{
        //调用删除的接口
        await deleteHosSetFormIdRequest(id)

        //重新渲染
        setReRender(!reRender)
      }
    }

    //5.批量删除按钮事件回调函数
    const batchDeleteHandle = async()=>{
      //发送批量删除请求
      await batchDeleteHosRequest(batchSelectIds)
      message.success("批量删除成功")

      //重新渲染
      setReRender(!reRender)

      //把批量删除按钮恢复
      setIsSelect(true)
    }


  return (
    <div className={style.outer}>
      {/* card: 通用卡片容器*/}
      <Card> 
      <Form name="basic" 
      wrapperCol={{ span: 30 }}
       layout="inline"
       onFinish={searchHosFinishHandle}
       >
        <Form.Item name="hosname">
          <Input placeholder="医院名称" />
        </Form.Item>

        <Form.Item name="hoscode">
          <Input placeholder="医院编号" />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit"  icon={<SearchOutlined />}>
            查询
          </Button>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="default" htmlType="reset">
            清空
          </Button>
        </Form.Item>
      </Form>

      <div className={style.mt}>
        <Button type="primary" className={style.mr} onClick={addHosHandle}>
          添加
        </Button>
        <Button type="primary" danger disabled={isSelect} onClick={batchDeleteHandle}>
          批量删除
        </Button>
      </div>

      {/* 表格部分 */}
      <Table
        className={style.mt}
        columns={columns}
        dataSource={tableData}
        scroll={{ x: 1200 }}
        bordered
        //在table中 要求dataSource中有key属性,但是如果真的没有key 则可以使用rowKey属性指定dataSource中的一个属性作为Key
        rowKey="id"
        loading={isLoading}
        rowSelection={{
          onChange(ids:Key[]){
            //设置控制批量删除按钮state的状态
            setIsSelect(!ids.length)
            //当选项框修改的时候,实时的修改保存所有被选中id的状态
            setBatchSelectIds(ids)
          }
        }}
        pagination={{
          //页码的配置:当前页码
          current,
          ///总数据的数量
          total,
          //每页条数
          pageSize,

          //展示更改每页条数的选项(默认大于50开启)
          pageSizeOptions: [2, 3, 4, 5, 6],

          //是否直接开启更新每页条数的选项
          showSizeChanger: true,

          //数据总个数
          showTotal(total){
            return `总共有${total}条数据`
          },
          onChange:pageChangeHandle
        }}
      />
      </Card>
    </div>
  );
}
