import React, { useEffect, useState,Key } from "react";
import { Row, Col, Card, Tree, Tag, Pagination, Table, Button,message } from "antd";
import { useNavigate, useParams } from "react-router-dom";
import { bookingScheduleListType, findScheduleListType, hosDepartmentListType } from "@/api/hospital/model/hospitalListTypes";
import { findScheduleListRequest, hosDepartmentListRequest, hosScheduleRuleRequest } from "@/api/hospital/hospitalList";

export default function HospitalSchedule() {

  const navigate = useNavigate()
  //接收params参数
  const {hoscode} = useParams()
  console.log(hoscode);

  //1.定义一个控制treeData(左边树形的数据)的state
  const [treeData,setTreeDate] = useState<hosDepartmentListType>([])

  //2.定义一个时间排班规则的当前页码
  const [page,setPage] = useState(1)

  //3.定义一个时间排班规则的每页条数
  const [limit,setLimit]  = useState(3);

  //4.定义当前选中的科室
  const [depCode,setDepCode] = useState("200040878")

  //5.定义当前科室的排版规则
  const [scheduleRule,setScheduleRule] = useState<bookingScheduleListType>([])
  
  //6.定义当前科室的排版规则总条数
  const [total,setTotal] = useState(0)

  //7.定义当前选中的时间
  const [workDate,setWorkDate] = useState("2022-04-28")

  //8.定义表格的数据
  const [sourceData,setSourceData] = useState<findScheduleListType>([])

  
  //1.组件初始加载的时候发送科室列表请求
  useEffect(()=>{
    const getDepartmentList = async() =>{
      const result  = await hosDepartmentListRequest(hoscode as string)
      console.log(result);

      //把获取到的科室的数据交给state
      setTreeDate(result)
    };
    getDepartmentList()
  },[])

  // //onselect:被选中时调用,参数为选中项的value(或key)值
  // const onselect = (selectedKeys:any,info:any)=>{
  //   console.log("selected",selectedKeys,info);
  // }

  // //点击复选框触发
  // const onCheck = (checkedKeys:any,info:any)=>{
  //   console.log("oncheck",checkedKeys,info);
  // }

  //2.组件初始加载的时候发送科室排版请求
  useEffect(()=>{
    const getScheduleRule = async()=>{
      const result = await hosScheduleRuleRequest({
        page:page,
        limit:limit,
        hoscode:hoscode as string,
        depcode:depCode
      })
      console.log(result); 
      setScheduleRule(result.bookingScheduleList)
      setTotal(result.total)

      //把数据中第一个时间交给wordDate这个状态的初始值
      setWorkDate(result.bookingScheduleList[0]?.workDate)
    }
    getScheduleRule()
  },[page,limit,depCode])

  //3.组件初始化的时候根据科室和时间发送详细信息请求
  useEffect(()=>{
    const getFindScheduleList = async()=>{
      const result = await findScheduleListRequest({
        hoscode:hoscode as string,
        depcode:depCode,
        workDate:workDate
      })
      setSourceData(result)
      // console.log(result);
      
    }
    getFindScheduleList()
  },[workDate,depCode])

  //1.改变页码的事件回调函数
  const changePageHandle=(page:number,limit:number)=>{
    setPage(page)
    setLimit(limit)
  }

  //2.点击tree的事件回调函数
  const treeSelectHandle = (depCodeArr:Key[])=>{
    setDepCode(depCodeArr[0] as string)
  }

  //3.点击事件的事件回调函数
  const scheduleRuleHandle = (workDate:string) =>{
    return ()=>{
      setWorkDate(workDate)
    }
  }
  
  
  //table表格结构
  const columns = [
    {
      title: "序号",
      render(_: any, __: any, index: number) {
        return index + 1;
      },
    },
    {
      title: "职称",
      dataIndex: "title",
    },
    {
      title: "号源时间",
      dataIndex: "workDate",
    },
    {
      title: "可预约数",
      dataIndex: "reservedNumber",
    },
    {
      title: "挂号费用",
      dataIndex: "amount",
    },
    {
      title: "擅长技能",
      dataIndex: "skill",
    },
  ];
  const treeStyle = {
    border: "1px solid #000",
    height: "480px",
    overflow: "auto",
  };
  return (
    <Card>
      <p>选择:北京协和医院/多发性硬化专科门诊/2022-04-08</p>
      <Row gutter={20}>
        <Col span={5}>
          <div>
            <Tree
             treeData={treeData as []}
             style={treeStyle} 
             //自定义节点 title、key、children 的字段
             fieldNames={{title:"depname",key:"depcode"}}
             expandedKeys = {[
              "a4e171f4cf9b6816acdfb9ae62c414d7",
             ]}
             onSelect={treeSelectHandle}
             />
          </div>
        </Col>
        <Col span={19}>
          <div style={{ marginBottom: "20px" }}>
            {
              scheduleRule.map((item)=>{
                return(
                  <Tag
                   key={item.workDate}
                   onClick={scheduleRuleHandle(item.workDate)}
                   style={{cursor:"pointer"}}
                   >

                    <p>{item.workDate}</p>
                    <p>
                      {item.availableNumber}/{item.reservedNumber}
                    </p>
                  </Tag>
                )
              })
            }
          </div>
          <Pagination 
          current={page}
          pageSize={limit}
          total={total}
          onChange= {changePageHandle}
          />

          <Table 
           columns={columns}
           style={{ marginTop: "20px" }}
           dataSource={sourceData}
           rowKey={"id"}
           bordered
           pagination={false}

           ></Table>

           <Button
           onClick={()=>{
            navigate(-1)
           }}
           style={{marginTop:"30px"}}
           ></Button>
        </Col>
      </Row>
    </Card>
  );
}