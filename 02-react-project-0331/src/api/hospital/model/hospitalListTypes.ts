//1. 请求医院列表 所有类型

import { type } from "os";

//请求医院列表参数的类型
export interface getHosListSearchParamsType {
  hoscode?: string;
  hosname?: string;
  hostype?: string;
  provinceCode?: string;
  cityCode?: string;
  districtCode?: string;
  status?: 0 | 1;
}

//定义数据中bookingRule的类型
export interface BookingRuleType {
  cycle: number;
  releaseTime: string;
  stopTime: string;
  quitDay: number;
  quitTime: string;
  rule: string[];
}

//请求医院列表的返回值中content数组中 每一条数据的类型
export interface hospitalListItemsType {
  id: string;
  createTime: string;
  updateTime: string;
  isDeleted: number;
  param: {
    hostypeString: string;
    fullAddress: string;
  };
  hoscode: string;
  hosname: string;
  hostype: string;
  provinceCode: string;
  cityCode: string;
  districtCode: string;
  address: string;
  logoData: string;
  intro: string;
  route: string;
  status: 0 | 1;
  bookingRule: BookingRuleType |null
}

//请求医院列表的返回值中content的数组类型
export type hospitalListContentType = hospitalListItemsType[];

//请求医院列表的返回值类型
export interface hospitalListReqReturnType {
  content: hospitalListContentType;
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
  totalElements: number;
  totalPages: number;
  last: boolean;
  first: boolean;
  sort: {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  };
  numberOfElements: number;
  size: number;
  number: number;
  empty: boolean;
}

//2.请求省份返回值类型
export interface getProvinceItemType {
  createTime: string;
  dictCode: string;
  hasChildren: boolean;
  id: number;
  isDeleted: number;
  name: string;
  param: object;
  parentId: number;
  updateTime: string;
  value: string;
}
export type getProvinceReturnType = getProvinceItemType[];

//3.医院详情的返回值类型
export interface hospitalDetailReturnType{
  "bookingRule"?:BookingRuleType,
  "hospital"?:hospitalListItemsType
}

//4.医院排版科室每一项的返回值类型
export interface hosDepartmentItemType{
  depcode:string,
  depname:string,
  children:hosDepartmentListType |null
}

export type hosDepartmentListType = hosDepartmentItemType[]

//5.某个科室的排版时间请求类型
//参数类型
export interface hosScheduleRuleParamsType{
  page:number;
  limit:number;
  hoscode:string;
  depcode:string
}

//返回值类型
export interface bookingScheduleListItem{
  workDate: string;
  workDateMd: null;
  dayOfWeek: string;
  docCount: number;
  reservedNumber: number;
  availableNumber: number;
  status: null;
}

export type bookingScheduleListType = bookingScheduleListItem[]

export interface hosScheduleRuleReturnType{
  total: number
  bookingScheduleList: bookingScheduleListType
  baseMap: {
    hosname: string
  };
}

//6.根据时间和科室获取详情
export interface findScheduleListParamsType{
  hoscode:string;
  depcode:string;
  workDate:string;
}

export interface findScheduleListItemType{
  id:string;
  createTime:string;
  update:string;
  isDeleted:number;
  param:{
    dayOfWeek: string;
    depname: string;
    hosname: string;
  };
  hoscode: string;
  depcode: string;
  title: string;
  docname: string;
  skill: string;
  workDate: string;
  workTime: number;
  reservedNumber: number;
  availableNumber: number;
  amount: number;
  status: 0 | 1;
  hosScheduleId: string;
}

export type findScheduleListType = findScheduleListItemType[]