import { request } from "@/utils/http";
import type {
  getHosListSearchParamsType,
  hospitalListReqReturnType,
  getProvinceReturnType,
  hospitalDetailReturnType,
  hosDepartmentListType,
  hosScheduleRuleParamsType,
  hosScheduleRuleReturnType,
  findScheduleListType,
  findScheduleListParamsType
} from "./model/hospitalListTypes";

//1.获取医院列表数据的请求
export const getHospitalListRequest = (
  page: number,
  limit: number,
  searchParams: getHosListSearchParamsType
) => {
    return request.get<any,hospitalListReqReturnType>(`/admin/hosp/hospital/${page}/${limit}`,{
        params:searchParams
    })
};

//2.请求所有的省份
export const getProvinceRequest = () =>{
  return request.get<any,getProvinceReturnType>("/admin/cmn/dict/findByDictCode/province")
}

//3.请求所有的城市
export const getCityOrDistrictRequest =(value:string)=>{
  return request.get<any,getProvinceReturnType>(`/admin/cmn/dict/findByParentId/${value}`)
}

//4.请求医院详情信息
export const getHosListDetailRequest = (id:string)=>{
  return request.get<any,hospitalDetailReturnType>(`/admin/hosp/hospital/show/${id}`)
}

//5.医院上下线
export const changeStatusRequest=(id:string,status:0|1)=>{
  return request.get<any,null>(`/admin/hosp/hospital/updateStatus/${id}/${status}`)
}

//6.请求所有科室的请求
export const hosDepartmentListRequest = (hoscode:string) =>{
  return request.get<any,hosDepartmentListType>(`/admin/hosp/department/${hoscode}`)
}

//7.获取某个科室的详细排版的时间请求
export const hosScheduleRuleRequest=({page,limit,hoscode,depcode}:hosScheduleRuleParamsType)=>{
  return request.get<any,hosScheduleRuleReturnType>(`/admin/hosp/schedule/getScheduleRule/${page}/${limit}/${hoscode}/${depcode}`)
}

//8.获取某个科室某个时间的具体详情(表格)
export const findScheduleListRequest = ({hoscode,depcode,workDate}:findScheduleListParamsType)=>{
  return request.get<any,findScheduleListType>(`/admin/hosp/schedule/findScheduleList/${hoscode}/${depcode}/${workDate}`)
}
