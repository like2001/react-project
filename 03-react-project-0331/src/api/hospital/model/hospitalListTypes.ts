//1. 请求医院列表 所有类型
//请求医院列表参数的类型
export interface getHosListSearchParamsType {
    hoscode?: string,
    hosname?: string,
    hostype?: string,
    provinceCode?: string,
    cityCode?: string,
    districtCode?: string,
    status?: 0 | 1
  }

  //定义数据中bookingRule的类型
export interface BookingRuleType {
  cycle: number;
  releaseTime: string;
  stopTime: string;
  quitDay: number;
  quitTime: string;
  rule: string[];
}
  
  //请求医院列表的返回值中content数组中 每一条数据的类型
  export interface hospitalListItemsType {
    id: string,
    createTime: string,
    updateTime: string,
    isDeleted: number,
    param: {
      hostypeString: string,
      fullAddress: string
    },
    hoscode: string,
    hosname: string,
    hostype: string,
    provinceCode: string,
    cityCode: string,
    districtCode: string,
    address: string,
    logoData: string,
    intro: string,
    route: string,
    status: 0 | 1,
    bookingRule:BookingRuleType|null
  }
  
  //请求医院列表的返回值中content的数组类型
 export type hospitalListContentType = hospitalListItemsType[];
  
  //请求医院列表的返回值类型
  export interface hospitalListReqReturnType {
    content: hospitalListContentType,
    pageable: {
      sort: {
        sorted: boolean,
        unsorted: boolean,
        empty: boolean
      },
      pageNumber: number,
      pageSize: number,
      offset: number,
      paged: boolean,
      unpaged: boolean
    },
    totalElements: number,
    totalPages: number,
    last: boolean,
    first: boolean,
    sort: {
      sorted: boolean,
      unsorted: boolean,
      empty: boolean
    },
    numberOfElements: number,
    size: number,
    number: number,
    empty: boolean
  }

  //2.请求省份返回值类型
  export interface getProvinceItemType{
    createTime: string;
    dictCode: string;
    hasChildren: boolean;
    id: number;
    isDeleted: number;
    name: string;
    param: object;
    parentId: number;
    updateTime: string;
    value: string;
  }

  export type getProvinceReturnType = getProvinceItemType[]

  //3.医院详情的返回值类型
  export interface hospitalDetailReturnType{
    "bookingRule"?: BookingRuleType,
    "hospital"?: hospitalListItemsType
  }