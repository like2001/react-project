import {request} from '@/utils/http'
import { Key } from 'react'


import {
    getHospitalSetListReturnType,
    getHospitalSetListParamsType,
    addHospitalParamType,
    getHospitalSetListItemsType,
    updateHospitalParamType
} from './model/hospitalSetTypes'

//医院设置中,要获取分页列表,请求数据
export const getHospitalSetListRequest=({page,limit,hosname,hoscode}:getHospitalSetListParamsType)=>{
    return request.get<any,getHospitalSetListReturnType>(`/admin/hosp/hospitalSet/${page}/${limit}`,{
        //配置对象
        params:{
            hosname,
            hoscode
        }
    })
}

//1.医院设置中,新增医院请求
export const addHospitalRequest=(data:addHospitalParamType)=>{
    return request.post<any,null>("/admin/hosp/hospitalSet/save", data)
}

//2.医院设置中,根据id获取某个详细的医院设置
export const getHosSetFromIdRequest= (id:string) =>{
    return request.get<any,getHospitalSetListItemsType>(`/admin/hosp/hospitalSet/get/${id}`)
}

//3.医院设置中,根据id修改数据后,再次提交修改
export const updateHosSetFromRequest = (data:updateHospitalParamType)=>{
    return request.put<any,null>("/admin/hosp/hospitalSet/update", data)
}

//4.医院设置中,根据id删除数据
export const deleteHosSetFormRequest=(id:number)=>{
    return request.delete<any,null>(`/admin/hosp/hospitalSet/remove/${id}`)
}

//5.医院设置中,批量删除数据
export const batchDeleteHosRequest = (ids:Key[])=>{
    return request.delete<any,null>(`/admin/hosp/hospitalSet/batchRemove`,{
        data:ids
    })
}