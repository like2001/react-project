import React, { useState,useEffect} from 'react'
import { Card, Form, Button, Input, Select, Table, Row, Col,message } from 'antd'
import { SearchOutlined } from "@ant-design/icons"

import { getHospitalListRequest, getProvinceRequest,getCityOrDistrictRequest,changeStatusRequest } from '@/api/hospital/hospitalList'
import type { 
  getHosListSearchParamsType,
  hospitalListContentType,
  hospitalListItemsType,
  getProvinceReturnType,
} from '@/api/hospital/model/hospitalListTypes'
import { useNavigate } from 'react-router-dom'



export default function HospitalList() {

  const [form] = Form.useForm()
  const navigate = useNavigate()

  //1.控制表格是否加载中的状态  
  const [isLoading, setIsLoading] = useState(false)

  //2.控制当前页码的状态
  const [page,setPage] = useState(1)

  //3.控制每页显示的条数
  const [limit,setLimit] = useState(3)

  //4.控制查询时用户输入的表单参数
  const [searchParams,setSearchParams] = useState<getHosListSearchParamsType>({})

  //5.控制医院总数量
  const [total,setTotal] = useState(0)

  //6.控制表格的数据
  const [hospitalList,setHospitalList] = useState<hospitalListContentType>([])

  //7.控制省份的数据
  const [province,setProvince] = useState<getProvinceReturnType>([])

  //8.控制市的数据
  const [cityList,setCityList] = useState<getProvinceReturnType>([])

  //9.控制区的数据
  const [district,setDistrict] = useState<getProvinceReturnType>([])

  //10.控制医院类型
  const [hospitalType,sethospitalType] =useState<getProvinceReturnType>([])

  //11.控制重新渲染的state
  const [reRender,setReRender] = useState(true)

  //1.刚加载组件的时候,发送医院列表的请求
  useEffect(()=>{
    setIsLoading(true)
    const getHospitalList = async()=>{
    const result=  await getHospitalListRequest(page,limit,searchParams)

    //把结果中的医院列表的数组设置状态state
    setHospitalList(result.content)

      //把结果中的医院总数设置给状态state
      setTotal(result.totalElements)
      setIsLoading(false)
    }
    getHospitalList()
  },[searchParams,page,limit,reRender])


  //2.初始化的时候,请求所有的省份
  useEffect(()=>{
    const getProvince = async()=>{
      const result = await getProvinceRequest()

      setProvince(result)
    }
    getProvince()
  },[])

  //3.初始化的时候立即请求医院类型
  useEffect(()=>{
    const getHospitalType = async()=>{
      const result = await getCityOrDistrictRequest("10000")
      //把医院的类型设置给状态
      sethospitalType(result)
    }
    getHospitalType()
  })


  //事件回调函数
  //1.选择省份的时候请求所有的市和区
    const changeProvinceHandle = async(value:string)=>{
      //根据省份的value发送市的请求
      const result = await getCityOrDistrictRequest(value)
      setCityList(result)

      //选择完省份的时候,把市和区目前的value清掉
      form.setFieldsValue({cityCode:null,districtCode:null})

      //清空县的数据
      setDistrict([])
    }

  //2.选择城市的时候,事件回调函数
    const cityChangeHandle= async(value:string)=>{
      //根据省份发送市的请求
      const result = await getCityOrDistrictRequest(value)
       console.log("result",result);
      //把结果给state
      setDistrict(result)
      
      //选择完省份的时候,把市和区目前的value去掉
      form.setFieldsValue({districtCode:null})
    }

  //3.查询表单的提交事件
  const finishHandle =async(value:getHosListSearchParamsType)=>{
      setSearchParams(value)
  }

  //4.点击查询表单的清空按钮
  const clearSearchHandle = ()=>{
    setSearchParams({})
  }

  //5.点击查看按钮事件回调函数
  const hospitalDetailHandle = (id:string) =>{
    return()=>{
    navigate(`/syt/hospital/hospitalShow/${id}`)
    }
  }

  //6.点击上下线的事件回调函数
  const changeStateHandle =(id:string,status:0|1)=>{
    return async()=>{
        await changeStatusRequest(id,status);
        message.success("改变医院上下线状态成功")

        //重新渲染
        setReRender(!reRender)
    }
  }

  

  const tableData: any[] = [];
  //表格的字段
  const columns = [
    {
      title: '序号',
      width: 60,
      dataIndex: 'index',
      render(_: any, __: any, index: number) {
        return index + 1
      }
    },
    {
      title: '医院logo',
      //如果没有dataIndex设置,则render中拿到的数据就是当前完整的数据
        //如果设置了dataIndex,则render拿到的dataIndex对应的数据
      dataIndex: 'logoData',
      render(logoData:string){
        return(
          <img width={100} src={`data:image/jpeg;base64,${logoData}`} alt="" />
        )
      }
    },
    {
      title: '医院名称',
      dataIndex:'hosname'
      },
    
    {
      title: '等级',
      render(_:any,data:hospitalListItemsType){
        return data.param.hostypeString
      }
    },
    {
      title: '详细地址',
      render(_:any,data:hospitalListItemsType){
        return data.param.fullAddress
      }
    },
    {
      title: '状态',
      render(_:any,data:hospitalListItemsType){
        return data.status===0?"下线":"上线"
      }
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
    },
    {
      title: '操作',
      width: 120,
      fixed: "right" as "right",
      dataIndex: "id",
      render(_:any,data:hospitalListItemsType) {
        return (
          <div>
            <Button type='primary' onClick={hospitalDetailHandle(data.id)}>查看</Button>
            <Button type='primary'>排班</Button>
            <Button type='primary'
              onClick={changeStateHandle(data.id,data.status ===0?1:0)}>
                {data.status === 0?"上线":"下线"}
            </Button>


          </div >
        )
      }
    }
  ];
  return (

    <div>
      <Card>
        <Form
          form={form}
          name="basic"
          onFinish={finishHandle}
        >
          <Row gutter={10}>
            <Col span={4}>
          <Form.Item
            name="provinceCode"
          >
            <Select
              placeholder="请输入省份"
              allowClear
              onChange={changeProvinceHandle}
            >
             {
                province.map((item)=>{
                  return(
                    <Select.Option value={item.value} key={item.id}>
                      {item.name}
                    </Select.Option>
                  )
                })
             }
            </Select>
          </Form.Item>
          </Col>

          <Col span={4}>
          <Form.Item
            name="cityCode"
       
          >
            <Select
              placeholder="请输入市"
              allowClear
              onChange={cityChangeHandle}
            >
             
             {
              cityList.map((item)=>{
                return(
                  <Select.Option value={item.value} key={item.id}>
                    {item.name}
                  </Select.Option>
                )

              })
             }
            </Select>
          </Form.Item>
          </Col>

          <Col span={4}>
          <Form.Item
            name="districtCode"
        
          >
            <Select
              placeholder="请输入县/区"
              allowClear
            >
             {
              district.map((item)=>{
                return(
                  <Select.Option value={item.value} key={item.id}>
                    {item.name}
                  </Select.Option>
                )
              })
             }
            </Select>
          </Form.Item>
          </Col>

          <Col span={4}>
          <Form.Item
            name="hosname"
       
          >
            <Input placeholder="医院名称" />
          </Form.Item>
          </Col>

          <Col span={4}>
          <Form.Item
            name="hoscode"
          
          >
            <Input placeholder='医院编号' />
          </Form.Item>
          </Col>

          <Col span={4}>
          <Form.Item
            name="hostype"
          >
            <Select
              placeholder="医院类型"
              allowClear
            >
             {
                  hospitalType.map((item)=>{
                    return(
                      <Select.Option value={item.value} key={item.id}>
                        {item.name}
                      </Select.Option>

                    )

                  })
                }
             
            </Select>
          </Form.Item>
          </Col>
          </Row>
          
            <Row gutter={10}>
              <Col span={4}>
          <Form.Item
            name="status"
          
          >
            <Select
              placeholder="医院状态"
              allowClear
            >
              <Select.Option value={1}>上线</Select.Option>
              <Select.Option value={0}>下线</Select.Option>
            </Select>
          </Form.Item>
          </Col>

          <Col span={2}>
          <Form.Item >
            <Button type="primary" htmlType="submit"  icon={<SearchOutlined />} >
              查询
            </Button>
          </Form.Item>
          </Col>

            <Col span={2} style={{ marginLeft: "20px" }}>
          <Form.Item>
            <Button type="default" htmlType="reset" onClick={clearSearchHandle} >
              清空
            </Button>
          </Form.Item>
          </Col>
          </Row>
        </Form>
     



        <Table
          style={{ marginTop: "30px" }}
          columns={columns}
          dataSource={hospitalList}
          scroll={{ x: 1100 }}
          bordered
          //在table中 要求dataSource中有key属性,但是如果真的没有key 则可以使用rowKey属性指定dataSource中的一个属性作为Key
          rowKey="id"
          loading={isLoading}

          pagination={{
            //页码的配置：当前页码
            current: page,
            //总数据的数量
            total: total,
            //每页条数
            pageSize: limit,
            //展示更改每页条数的选项(默认total大于50条的时候开启)
            pageSizeOptions: [3, 8, 15],
            //是否直接开启更新每页条数的选项
            showSizeChanger: true,

            showTotal(total) {
              return `总共有${total}条数据`
            },
            onChange(page:number,pageSize:number){
              setPage(page)
              setLimit(pageSize)
            }
          }}
        />
      </Card>
    </div >
  )
}