import React, { useEffect, useState } from "react";
import { Descriptions, Card, Button } from "antd";
import { getHosListDetailRequest } from "@/api/hospital/hospitalList";
import { useParams ,useNavigate} from "react-router-dom";
import { BookingRuleType, hospitalDetailReturnType, hospitalListItemsType } from "@/api/hospital/model/hospitalListTypes";

export default function HospitalShow() {
  const {id} = useParams();

  //书写一个保存详细信息的状态
  const [detail,setDetail] = useState<hospitalDetailReturnType>({})
  //刚加载组件就发送详情数据请求
  useEffect(()=>{
    const getHosDetail = async()=>{
      const result = await getHosListDetailRequest(id as string)
      console.log(result);
      
      //把数据设置给state
      setDetail(result)
    }
    getHosDetail()
  },[]);

  //1.返回
  const navigate = useNavigate();
  const goBack=()=>{
    navigate("/syt/hospital/hospitalList")
  }

  return (
    <Card>
      <Descriptions title="基本信息" bordered column={2}>
        <Descriptions.Item label="医院名称">
        {(detail.hospital as hospitalListItemsType)?.hosname}
        </Descriptions.Item>

        <Descriptions.Item label="医院logo">
        {(detail.hospital as hospitalListItemsType)?.logoData?(
          <img 
          width={100}
          src={`data:image/jpeg;base64,${
            (detail.hospital as hospitalListItemsType)?.logoData
          }`}
          alt=""
          />
        ):(""
        )}
        </Descriptions.Item>

        <Descriptions.Item label="医院编码">
        {(detail.hospital as hospitalListItemsType)?.hoscode}
        </Descriptions.Item>

        <Descriptions.Item label="医院地址">
        {(detail.hospital as hospitalListItemsType)?.param?.fullAddress}
        </Descriptions.Item>

        <Descriptions.Item label="医院坐车路线" span={2}>
        {(detail.hospital as hospitalListItemsType)?.route}
        </Descriptions.Item>
        <Descriptions.Item label="医院简介" span={2}>
        {(detail.hospital as hospitalListItemsType)?.intro}
        </Descriptions.Item>
      </Descriptions>

      <Descriptions
        title="预约规则信息"
        bordered
        column={2}
        style={{ marginTop: "30px" }}
      >
        <Descriptions.Item label="预约周期">
        {(detail.bookingRule as BookingRuleType)?.cycle}
        </Descriptions.Item>
        <Descriptions.Item label="放号时间">
        {(detail.bookingRule as BookingRuleType)?.releaseTime}
          
        </Descriptions.Item>
        <Descriptions.Item label="停挂时间">
        {(detail.bookingRule as BookingRuleType)?.stopTime}
          
        </Descriptions.Item>
        <Descriptions.Item label="退号时间">
        {(detail.bookingRule as BookingRuleType)?.quitTime}
          
        </Descriptions.Item>
        <Descriptions.Item label="预约规则" span={2}>
        {(detail.bookingRule as BookingRuleType)?.rule.map((item,index)=>{
          return <p key={index}>{item}</p>
        })}

        </Descriptions.Item>
      </Descriptions>

      <Button style={{ marginTop: "30px" }} onClick={goBack}>返回</Button>
    </Card>
  );
}